# CallBilling API

This project has the endpoints required for requesting a billing of a calls. Implenting DRY, TDD, and the best possible good practices for web developing.

## Installation

ruby  -v: 2.7.0p0
rails -v: 6.0.4.4

```bash
bin/setup
```
or

```bash
bundle install
rails db:setup or rails db:create db:migrate
```

for running the unit test:
rspec
```json
  Finished in 0.68534 seconds (files took 3.41 seconds to load)
  42 examples, 0 failures
```
## Usage
these are some of examples.

## Endpoints Availables

| Name       | API Version | Methods             |
| -----------| ------------| --------------------|
| users      | v1          | index, show, create |
| calls      | v1          | create              |
| billings   | v1          | index               |

## Resources

### User
A user is the person who can make calls

### Call
It's the action realized by a user from a phone_number to a destinity.

### Billing
It's the bill by month an user according to the rate of the calls.

condiderations: for requet a billing in the body we have to send the number of the month what we have to see. Ex: [january: 1, febrary:2 ...]

## Endpoints

### User

#### Get a user

- Method: `GET`

- URL: `api/v1/users/<phone_number>`

- Request Example: N/A

- Response Example:

  ```json
  {
    "first_name": "alvaro",
    "last_name": "alvarez",
    "phone_number": "112233445566",
    "document_number": "123456789"
  }
  ```
#### Create a user

- Method: `POST`

- URL: `api/v1/users`

- Request Example:

  ```json
  {
    "first_name": "alvaro",
    "last_name": "alvarez",
    "phone_number": "112233445566",
    "document_number": "123456789"
  }
  ```

- Response Example:

  ```json
  {
    "first_name": "alvaro",
    "last_name": "alvarez",
    "phone_number": "112233445566",
    "document_number": "123456789"
  }
  ```

### Call

#### Create a call
- Method: `POST`

- URL: `api/v1/users/<phone_number>/calls`

- Request Example:

  ```json
  {
    "minutes": "100",
    "call_type": "national",
    "destination": "Barranquilla"
  }
  ```

- Response Example:

  ```json
  {
    "minutes": "1000",
    "call_type": "national",
    "destination": "Barranquilla",
    "call_date": "020-12-03T21:31:32.584-05:00",
    "rate": "100.0"
  }
  ```

### Bill

#### Get a bill
- Method: `POST`

- URL: `api/v1/users/<phone_number>/billings`

- Request Example:

  ```json
  {
    "month": 1
  }
  ```

- Response Example:

  ```json
  {
    "total_rate": 50.15,
    "national_minutes": 50,
    "international_minutes": 10
  }
  ```

## License
[MIT](https://choosealicense.com/licenses/mit/)