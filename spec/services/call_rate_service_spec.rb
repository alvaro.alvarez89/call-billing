# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CallRateService, type: :service do
  subject(:service) { described_class.new(call: call, params: params, user: user) }
  let(:call) { Call.new }
  let(:user) { users(:base) }

  let(:params) do
    {
      minutes: 50,
      call_type: 'national',
      destination: '3023515886'
    }
  end

  describe '#execute!' do
    context 'when are all the parameters' do
      context 'and there is a national call_type' do
        it 'creates and persists a new Call in the database' do
          expect { subject.execute! }.to change(Call, :count).by(1)
        end
      end

      it 'returns true' do
        expect(subject.execute!).to be true
      end

      context 'when the call can\'t be persisted' do
        before do
          params[:call_type] = nil
        end

        it 'returns an ActiveRecord::ActiveRecordError exception' do
          expect { subject.execute! }.to raise_exception ActiveRecord::ActiveRecordError
        end
      end
    end
  end
end
