# frozen_string_literal: true

require 'rails_helper'

RSpec.describe BillingService, type: :service do
  subject(:service) { described_class.new(calls: [call, call]) }
  let(:call) { calls(:base) }

  let(:params) do
    {
      month: 1
    }
  end

  describe '#execute' do
    context 'when are all the parameters' do
      it 'returns a hash object with the bill' do
        expect(subject.execute).to be_an(Hash)
      end

      it 'includes total_rate key' do
        expect(subject.execute).to have_key(:total_rate)
      end

      it 'includes total_rate key' do
        expect(subject.execute).to have_key(:national_minutes)
      end

      it 'includes total_rate key' do
        expect(subject.execute).to have_key(:international_minutes)
      end
    end
  end
end
