# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Show Billings', type: :request do
  let(:user_phone_number) { users(:base).phone_number }
  subject(:request_billing) do
    get "/api/v1/users/#{ user_phone_number }/billings", params: { month: 1 }
  end

  context 'when the request has valid params' do
    it 'returns a created status' do
      request_billing
      expect(response).to have_http_status :ok
    end

    it 'matches the JSON schema' do
      request_billing
      expect(response_body).to match_json_schema('api/v1/billing')
    end
  end
end
