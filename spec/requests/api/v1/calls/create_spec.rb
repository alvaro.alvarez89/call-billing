# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Create a call', type: :request do
  let(:user_phone_number) { users(:base).phone_number }
  let(:payload) { load_dynamic_json_symbolized('payloads/create_call.json') }
  subject(:create_call) do
    post "/api/v1/users/#{ user_phone_number }/calls", params: payload
  end

  context 'when the request has valid params' do
    it 'returns a created status' do
      create_call
      expect(response).to have_http_status :created
    end

    it 'creates a user' do
      expect { create_call }
        .to change(Call, :count).by(1)
    end

    it 'matches the JSON schema' do
      create_call
      expect(response_body).to match_json_schema('api/v1/call')
    end
  end

  context 'when the request has invalid params' do
    context 'and the minutes is nil' do
      before { payload[:minutes] = nil }

      it 'returns a minutes error' do
        create_call
        expect(response_body[:errors].first[:attribute]).to eq 'minutes'
      end
    end
  end

  context 'when the request has invalid params' do
    context 'and the document_number is nil' do
      before { payload[:call_type] = nil }

      it 'returns a call type error' do
        create_call
        expect(response_body[:errors].first[:attribute]).to eq 'call_type'
      end
    end
  end
end
