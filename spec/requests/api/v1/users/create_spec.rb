# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Create a user', type: :request do
  let(:payload) { load_dynamic_json_symbolized('payloads/create_user.json') }
  subject(:create_user) do
    post '/api/v1/users', params: payload
  end

  context 'when the request has valid params' do
    it 'returns a created status' do
      create_user
      expect(response).to have_http_status :created
    end

    it 'creates a user' do
      expect { create_user }
        .to change(User, :count).by(1)
    end

    it 'matches the JSON schema' do
      create_user
      expect(response_body).to match_json_schema('api/v1/user')
    end
  end

  context 'when the request has invalid params' do
    context 'and the phone_number is nil' do
      before { payload[:phone_number] = nil }

      it 'returns a phone number error' do
        create_user
        expect(response_body[:errors].first[:attribute]).to eq 'phone_number'
      end
    end
  end

  context 'when the request has invalid params' do
    context 'and the document_number is nil' do
      before { payload[:document_number] = nil }

      it 'returns a phone number error' do
        create_user
        expect(response_body[:errors].first[:attribute]).to eq 'document_number'
      end
    end
  end
end
