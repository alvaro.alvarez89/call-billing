# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Show a user', type: :request do
  let(:user) { users(:base) }
  subject(:request) do
    get "/api/v1/users/#{ user.phone_number }"
  end

  describe '.show' do
    context 'when the user does not exist' do
      before do
        user.phone_number = 'not_exists'
        request
      end

      it 'returns a not_found status' do
        expect(response).to have_http_status :not_found
      end
    end

    context 'when the user exists' do
      before { request }

      it 'returns an ok status' do
        expect(response).to have_http_status :ok
      end

      it 'matches with the JSON schema' do
        expect(response_body).to match_json_schema('/api/v1/user')
      end
    end
  end
end
