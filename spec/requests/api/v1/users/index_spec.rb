# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Users Index', type: :request do
  subject(:request) do
    get '/api/v1/users'
  end

  describe '.index' do
    context 'when the users exists' do
      before { request }

      it 'returns an ok status' do
        expect(response).to have_http_status :ok
      end

      it 'returns an array of cancellations' do
        expect(response_body).to be_an(Array)
      end

      it 'matches with the JSON schema' do
        expect(response_body).to match_json_schema('/api/v1/users')
      end
    end
  end
end
