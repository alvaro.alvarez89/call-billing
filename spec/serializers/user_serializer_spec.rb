# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserSerializer, type: :serializer do
  let(:model) { users(:base) }
  subject { described_class.new(model).as_json }

  it 'builds the attributes' do
    expect(subject[:first_name]).to eq(model.first_name)
    expect(subject[:last_name]).to eq(model.last_name)
    expect(subject[:phone_number]).to eq(model.phone_number)
    expect(subject[:document_number]).to eq(model.document_number)
  end
end
