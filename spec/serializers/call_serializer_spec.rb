# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CallSerializer, type: :serializer do
  let(:model) { calls(:base) }
  subject { described_class.new(model).as_json }

  it 'builds the attributes' do
    expect(subject[:minutes]).to eq(model.minutes)
    expect(subject[:call_type]).to eq(model.call_type)
    expect(subject[:destination]).to eq(model.destination)
    expect(subject[:call_date]).to eq(model.call_date)
    expect(subject[:rate]).to eq(model.rate)
  end
end
