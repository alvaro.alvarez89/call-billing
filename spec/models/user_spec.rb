# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  it { should have_many(:calls) }

  describe 'Validations' do
    subject { users(:base) }

    it 'validates attributes uniqueness' do
      should validate_uniqueness_of(:phone_number)
    end

    it 'has a first name error' do
      subject.first_name = nil
      subject.valid?
      expect(subject.errors).to include(:first_name)
    end

    it 'has a last name error' do
      subject.last_name = nil
      subject.valid?
      expect(subject.errors).to include(:last_name)
    end

    it 'has a phone number error' do
      subject.phone_number = nil
      subject.valid?
      expect(subject.errors).to include(:phone_number)
    end

    it 'has a document number error' do
      subject.document_number = nil
      subject.valid?
      expect(subject.errors).to include(:document_number)
    end

    it 'is invalid without a first name' do
      subject.first_name = nil
      subject.valid?
      expect(subject).not_to be_valid
    end

    it 'is invalid without a last name' do
      subject.last_name = nil
      subject.valid?
      expect(subject).not_to be_valid
    end

    it 'is invalid without a phone number' do
      subject.phone_number = nil
      subject.valid?
      expect(subject).not_to be_valid
    end

    it 'is invalid without a document number' do
      subject.document_number = nil
      subject.valid?
      expect(subject).not_to be_valid
    end
  end
end
