# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Call, type: :model do
  it { should belong_to(:user) }

  describe 'Validations' do
    subject { calls(:base) }

    it 'is invalid without minutes' do
      subject.minutes = nil
      subject.valid?
      expect(subject.errors).to include(:minutes)
    end

    it 'is invalid without a call_type' do
      subject.call_type = nil
      subject.valid?
      expect(subject.errors).to include(:call_type)
    end

    it 'is invalid without a destination' do
      subject.destination = nil
      subject.valid?
      expect(subject.errors).to include(:destination)
    end

    it 'is invalid without a rate' do
      subject.rate = nil
      subject.valid?
      expect(subject.errors).to include(:rate)
    end
  end
end
