# frozen_string_literal: true

module Api
  module V1
    class BillingsController < ApplicationController
      before_action :find_calls, only: :index

      def index
        bill = BillingService.new(calls: @calls).execute

        render json: bill, status: :ok
      end

      private

      def find_calls
        @calls = Call.by_phone_number(
          params[:user_phone_number]
        ).by_month(
          params[:month]
        )
      end
    end
  end
end
