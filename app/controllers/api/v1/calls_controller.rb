# frozen_string_literal: true

module Api
  module V1
    class CallsController < ApplicationController
      before_action :find_user, only: :create

      # POST /calls
      def create
        call = Call.new
        service = CallRateService.new(call: call, params: call_params, user: @user)

        service.execute!

        render json: call, status: :created
      rescue ActiveRecord::ActiveRecordError
        render json: { errors: api_messages(call.errors.messages) }, status: :unprocessable_entity
      end

      private

      def find_user
        @user = User.find_by!(phone_number: params[:user_phone_number])
      end

      def call_params
        params.permit(:minutes, :call_type, :destination)
      end
    end
  end
end
