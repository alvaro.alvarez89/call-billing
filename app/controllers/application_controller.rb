# frozen_string_literal: true

class ApplicationController < ActionController::API
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  rescue_from ActiveRecord::RecordInvalid, with: :unprocessable_entity
  rescue_from ActionController::ParameterMissing, with: :unprocessable_entity

  private

  def record_not_found
    render :nothing, status: :not_found
  end

  def unprocessable_entity
    render :nothing, status: __method__
  end

  def api_messages(messages)
    messages.map do |attribute, error_description|
      message = { attribute: attribute.to_s, messages: [] }

      error_description.each_with_index do |description, index|
        code = details[attribute][index][:error].to_s if respond_to?(:details)
        message[:messages] << { code: code, description: description }
      end

      message
    end
  end
end
