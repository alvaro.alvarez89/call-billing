# frozen_string_literal: true

class BillingService
  def initialize(calls:)
    @calls = calls
  end

  def execute
    build_billing_operations!

    {
      total_rate: @calls.sum(&:rate),
      national_minutes: @national.sum(&:minutes),
      international_minutes: @international.sum(&:minutes)
    }
  end

  private

  attr_reader :calls

  def build_billing_operations!
    @national = []
    @international = []

    calls.each do |call|
      @national << call if call.national?
      @international << call if call.international?
    end
  end
end
