# frozen_string_literal: true

class CallRateService
  attr_reader :call, :params, :user

  def initialize(call:, params:, user:)
    @call = call
    @params = params
    @user = user
  end

  def execute!
    call.assign_attributes(params)
    call.user = user
    call.assign_current_date!
    call.calculate_rate!

    call.save!
  end
end
