# frozen_string_literal: true

class UserSerializer < ActiveModel::Serializer
  attributes :first_name, :last_name, :phone_number, :document_number
end
