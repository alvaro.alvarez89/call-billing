# frozen_string_literal: true

class CallSerializer < ActiveModel::Serializer
  attributes :minutes, :call_type, :destination, :call_date, :rate
end
