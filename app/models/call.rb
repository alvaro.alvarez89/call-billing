# frozen_string_literal: true

class Call < ApplicationRecord
  # TODO: We will need to use a dynamics range but for this implementation is enough have a constant RANGE_RATES
  RANGE_RATES = {
    min: 8,
    max: 20
  }.freeze

  enum call_type: %i[
    national
    international
  ]

  belongs_to :user

  validates :minutes, :call_type, :destination, :call_date, :rate, presence: true

  scope :by_month, ->(date) { joins(:user).where("cast(strftime('%m', call_date) as int) = ?", date) }
  scope :by_phone_number, ->(phone_number) { joins(:user).where('phone_number IN (?)', phone_number) }

  # Public: Used calculate the rate for a call, this method validate if is weekend alse the ranges for a rate
  #
  # Usage:
  #
  # Call.calculate_rate!
  #
  # Returns `true` when the transaction was successful and raises the exception when it wasn’t
  def calculate_rate!
    call_invalid if minutes.nil?
    weekend? ? self.rate = minutes * 0.10 : range_of_rate!
  end

  def assign_current_date!
    self.call_date ||= DateTime.now
  end

  private

  def weekend?
    call_date.saturday? || call_date.sunday?
  end

  def range_of_rate!
    self.rate ||= if call_date.hour.between?(RANGE_RATES[:min], RANGE_RATES[:max])
      minutes * 0.20
    else
      minutes * 0.10
    end
  end

  def call_invalid
    self.errors.add(
      :minutes,
      message: "can't be blank"
    )
    raise ActiveRecord::ActiveRecordError
  end
end
