# frozen_string_literal: true

class User < ApplicationRecord
  has_many :calls

  validates :first_name, :last_name, :phone_number, :document_number, presence: true
  validates_uniqueness_of :phone_number
end
