Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :users, param: :phone_number do
        resources :calls, only: :create
        resources :billings, only: :index
      end
    end
  end
end
