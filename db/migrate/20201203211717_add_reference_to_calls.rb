class AddReferenceToCalls < ActiveRecord::Migration[6.0]
  def change
    add_reference :calls, :user, index: true, foreign_key: true
  end
end
