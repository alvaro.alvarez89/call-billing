class CreateCalls < ActiveRecord::Migration[6.0]
  def change
    create_table :calls do |t|
      t.integer :minutes, null: false
      t.integer :call_type, null: false
      t.float :rate, null: false
      t.string :destination, null: false
      t.datetime :call_date, null: false

      t.timestamps
    end
  end
end
