class AddIndexPhoneNumberToUser < ActiveRecord::Migration[6.0]
  def change
    add_index :users, [:phone_number, :document_number]
  end
end
